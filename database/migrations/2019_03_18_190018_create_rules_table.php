<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateRulesTable extends Migration
{
  /**
   * Run the migrations.
   *
   * @return void
   */
  public function up()
  {
    Schema::create('rules', function (Blueprint $table) {
      $table->increments('id');
      //$table->string('operator', 4);
      //$table->unsignedInteger('consecuent')->index();
      $table->string( 'raw_original', 181 )->unique();
      $table->string( 'parsed_rule' );
      $table->timestamps();
    });
  }

  /**
   * Reverse the migrations.
   *
   * @return void
   */
  public function down()
  {
    Schema::dropIfExists('rules');
  }
}
