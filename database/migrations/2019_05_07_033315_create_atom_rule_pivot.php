<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAtomRulePivot extends Migration
{
  /**
   * Run the migrations.
   *
   * @return void
   */
  public function up()
  {
    Schema::create('atom_rule', function (Blueprint $table) {
      $table->increments( 'id' );
      $table->unsignedInteger( 'atom_id' );
      $table->unsignedInteger( 'rule_id' );
      $table->boolean( 'consecuent' )->default( 0 );

      $table->foreign( 'atom_id' )->references('id')->on('atoms');
      $table->foreign( 'rule_id' )->references('id')->on('rules');
    });
  }

  /**
   * Reverse the migrations.
   *
   * @return void
   */
  public function down()
  {
    Schema::dropIfExists('atom_rule');
  }
}
