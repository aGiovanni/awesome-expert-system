<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAtomsTable extends Migration
{
  /**
   * Run the migrations.
   *
   * @return void
   */
  public function up()
  {
    Schema::create('atoms', function (Blueprint $table) {
      $table->increments('id');
      $table->string( 'name', 181 )->unique();
      $table->boolean( 'value' )->default( 2 );
      $table->boolean( 'final' )->default( 0 );
      $table->timestamps();
    });
  }

  /**
   * Reverse the migrations.
   *
   * @return void
   */
  public function down()
  {
    Schema::dropIfExists('atoms');
  }
}
