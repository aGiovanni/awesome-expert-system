<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It is a breeze. Simply tell Lumen the URIs it should respond to
| and give it the Closure to call when that URI is requested.
|
| Get -> returns data
| POST -> CREATE / INSERT data
| PUT -> UPDATE also can DELETE
| PATCH -> UPDATE can NOT DELETE
| DELETE -> DELETE data
|
*/

$router->group([ 'prefix' => 'atoms' ], function() use ($router) {
  $router->get('/', 'AtomController@index');
  $router->get('/{id}', 'AtomController@show');

  $router->post('/', 'AtomController@store');
  $router->patch('/{id}', 'AtomController@update');
  $router->delete('/{id}', 'AtomController@destroy');

  $router->get('/{id}/search', 'AtomController@search');
});

$router->group([ 'prefix' => 'rules' ], function() use ($router) {
  $router->get('/', 'RuleController@show');
  $router->get('/{id}', 'RuleController@show');

  $router->post('/', 'RuleController@store');
  $router->patch('/{id}', 'RuleController@update');
  $router->delete('/{id}', 'RuleController@destroy');
});
