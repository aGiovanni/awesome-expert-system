<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Atom extends Model {

  /**
   * The attributes that are mass assignable.
   *
   * @var array
   */
  protected $fillable = [
    'name', 'value', 'final',
  ];

  public function rules() {
    return $this->belongsToMany( Rule::class )->withPivot( 'consecuent' );
  }

  public static function identify( $rule, $check_consecuents = true ) {
    // Identify all atoms
    $pattern = '/([a-qs-uw-z]+\d*)/';
    preg_match_all( $pattern, $rule, $matches );
    $atoms = $matches[1];

    if ( $check_consecuents ) {
      // Identify only consecuents
      $fpattern = '/->\s?([a-zA-Z^+*()]+\d?)\s?$/';
      preg_match_all( $fpattern, $rule, $matches );
      $consecuents = [];
      if ( sizeof( $matches[1] ) ) {
        preg_match_all( $pattern, $matches[1][0], $final_atoms);
        $consecuents = $final_atoms[1];
        $atoms = array_diff( $atoms, $consecuents );
      }

      return [
        'atoms' => $atoms,
        'consecuents' => $consecuents,
      ];
    }

    return $atoms;

  }

}
