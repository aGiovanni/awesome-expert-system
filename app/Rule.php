<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Rule extends Model {

  /**
   * The attributes that are mass assignable.
   *
   * @var array
   */
  protected $fillable = [
    'raw_original', 'parsed_rule',
  ];

  public function atoms() {
    return $this->belongsToMany( Atom::class )->withPivot( 'consecuent' );
  }

  public static function cleanSimbols( $rule ) {
    $rule = preg_replace( '/\s/', '', $rule );
    $rule = preg_replace( '/->/', '>', $rule );
    $rule = preg_replace( '/\^|\,|\&/', '*', $rule );
    $rule = preg_replace( '/v|\|/', '+', $rule );
    $rule = preg_replace( '/-|\'|~/', '!', $rule );
    return $rule;
  }

  public static function simplify( $rule ) {
    // Reorder negations
    $rule = preg_replace( '/((\w+\d*)(~))/', '!$2', $rule );
    // Remove Implication
    $rule = preg_replace( '/(.+)\>(.+)/U', '!($1)+$2', $rule );
    // Insert product to addition
    $rule = preg_replace( '/(\((\w+\d?)\+(\w+\d?)\))\*(\w+\d?)/U', '($2*$4)+($3*$4)', $rule );
    // Insert negations
    $rule = preg_replace( '/\!\(\((.+)\)(.)\((.+)\)\)/U', '!($1)$2!($3)', $rule );
    // Apply Morgan
    $rule = preg_replace( '/\!\((\!?\w+\d?)\+(\!?\w+\d?)\)/U', '(!$1*!$2)', $rule );
    $rule = preg_replace( '/\!\!/', '', $rule );
    return $rule;
  }

  public static function parseRule( $rule ) {
    $rules = [];
    $sub_rules = [];
    $rule = Self::cleanSimbols( $rule );
    // Get Sub strings
    $pattern = '/(\(\!?\w+.+\w+\))/U';
    $cont = 0;
    while ( preg_match( $pattern, $rule, $matches ) ) {
      array_push( $sub_rules, $matches[1] );
      $rule = preg_replace( $pattern, "R$cont", $rule, 1 );
      array_push( $rules, $rule );
      $cont++;
      if ( $cont > 20 ) { break; }
    }
    // Simplify sub-rules into original
    $size = sizeof( $rules );
    if ( $size > 0 ) {
      $rule = $rules[ sizeof( $rules ) - 1 ]; 
      for ( $i = sizeof( $sub_rules ) - 1; $i >= 0; $i-- ) {
        $rule = Self::simplify( $rule );
        if ( preg_match( "/R$i/", $rule ) ) {
          $sub_rules[ $i ] = Self::simplify( $sub_rules[ $i ] );
          $rule = preg_replace( "/R$i/", $sub_rules[ $i ], $rule );
          $rule = Self::simplify( $rule );
        }
      }
    } else { $rule = Self::simplify( $rule ); }
    return $rule;
  }

  public static function solve( $rule ) {
    // $atoms = Atom::identify( $rule, false );
    $equation = $rule->parsed_rule;
    $atoms = $rule->atoms()->get();
    foreach ( $atoms as $atom ) {
      if ( $atom->pivot->consecuent == 1 ) {
        $equation = preg_replace( "/$atom->name/", '0', $equation );
        continue;
      }
      if ( $atom->value == 2 ) { return false; }
      $equation = preg_replace( "/$atom->name/", $atom->value, $equation );
    }
    $result = eval( "return $equation ? 1 : 0;" );
    foreach ( $atoms as $atom ) {
      if ( $atom->pivot->consecuent == 1 ) {
        $atom->update([ 'value' => $result, ]);
      }
    }
    return [
      'rule' => $equation,
      'result' => $result,
    ];
  }

}
