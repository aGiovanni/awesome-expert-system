<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class AtomRule extends Model {

  protected $table = 'atom_rule';
  public $timestamps = false;

  /**
   * The attributes that are mass assignable.
   *
   * @var array
   */
  protected $fillable = [
    'atom_id', 'rule_id', 'consecuent',
  ];

  public function atom() {
    return $this->hasMany( Atom::class );
  }

  public function rule() {
    return $this->hasMany( Rule::class );
  }

  public static function updateFinals() {
    $pivots = AtomRule::where( 'consecuent', 1 )->get();
    foreach ( $pivots as $pivot ) {
      $count = AtomRule::where( 'atom_id', $pivot->atom_id )->where( 'consecuent', 0 )->count();
      if ( $count == 0 ) {
        Atom::find( $pivot->atom_id )->update([ 'final' => true, ]);
      } else {
        Atom::find( $pivot->atom_id )->update([ 'final' => false, ]);
      }
    }
  }

}
