<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Rule;
use App\Atom;
use App\AtomRule;

class RuleController extends Controller {

  private $request;

  public function __construct(Request $request) {
    $this->request = $request;
  }

  private function calcRule( $rule ) {
    $out_data = Rule::solve( $rule );
    $response = [ 'rule' => $rule->parsed_rule, ];
    if ( $out_data ) {
      // $response[ 'parsed' ] = $out_data[ 'rule' ];
      $response[ 'result' ] = $out_data[ 'result' ];
    }
    return $response;
  }

  private function saveOrUpdateAtoms( $rule ) {
    $atoms = Atom::identify( $rule );
    foreach ( $atoms[ 'atoms' ] as $key => $raw_atom ) {
      $atom = Atom::where( 'name', $raw_atom )->first();
      if ( !$atom ) {
        $atom = new Atom([ 'name' => $raw_atom, ]);
        $atom->save();
      }
      $atoms[ 'atoms' ][ $key ] = $atom->id;
    }
    foreach ( $atoms[ 'consecuents' ] as $key => $raw_consecuent ) {
      $atom = Atom::where( 'name', $raw_consecuent )->first();
      if ( !$atom ) {
        $atom = new Atom([ 'name' => $raw_consecuent, ]);
        $atom->save();
      }
      $atoms[ 'consecuents' ][ $key ] = $atom->id;
    }
    return $atoms;
  }

  public function index() {
    $rules = Rule::all();
    $response = [];
    foreach ( $rules as $rule ) {
      $parsed = $this->calcRule( $rule );
      // $parsed[ 'atoms' ] = $rule->atoms()->get();
      array_push( $response, $parsed );
    }
    return response()->json( $response, 200 );
  }

  public function show( $id ) {
    $rule = Rule::findOrFail( $id );
    $response = $this->calcRule( $rule );
    return response()->json( $response, 200 );
  }

  public function store() {
    $this->validate( $this->request, [
      'rule' => 'required|string|min:3|max:181',
    ]);
    $rule = new Rule([ 'raw_original' => $this->request->rule, ]);
    $atoms = $this->saveOrUpdateAtoms( $rule->raw_original );
    $rule->parsed_rule = Rule::parseRule( $rule->raw_original );
    $rule->save();
    $rule->atoms()->sync( $atoms[ 'atoms' ] );
    foreach ( $atoms[ 'consecuents' ] as $consecuent ) {
      $pivot = new AtomRule([
        'atom_id' => $consecuent,
        'rule_id' => $rule->id,
        'consecuent' => 1,
      ]);
      $pivot->save();
    }
    AtomRule::updateFinals();
    return response()->json([
      'id' => $rule->id,
      'result' => $rule->parsed_rule,
    ], 200 );
  }

  public function update( $id ) {
    $rule = Rule::findOrFail( $id );
    $atoms = $this->saveOrUpdateAtoms( $rule->raw_original );
    $rule->parsed_rule = Rule::parseRule( $rule->raw_original );
    $rule->save();
    $rule->atoms()->sync( $atoms[ 'atoms' ] );
    foreach ( $atoms[ 'consecuents' ] as $consecuent ) {
      $pivot = AtomRule::where( 'atom_id', $consecuent )->where( 'rule_id', $rule->id )->first();
      if ( !$pivot ) {
        $pivot = new AtomRule([
          'atom_id' => $consecuent,
          'rule_id' => $rule->id,
          'consecuent' => 1,
        ]);
        $pivot->save();
      }
    }
    AtomRule::updateFinals();
    return response()->json([ 'result' => $rule->parsed_rule, ], 200 );
  }

  public function destroy( $id ) {
    return $this->base_delete( Rule::class, $id );
  }

}
