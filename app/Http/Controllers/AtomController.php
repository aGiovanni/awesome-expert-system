<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Atom;
use App\Rule;
use App\AtomRule;

class AtomController extends Controller {

  private $request;

  private $atomList = [];
  private $ruleList = [];

  public function __construct(Request $request) {
    $this->request = $request;
  }

  public function index() {
    $atoms = Atom::all();
    foreach ( $atoms as $atom ) {
      $atom[ 'rules' ] = AtomRule::where( 'atom_id', $atom->id )->get();
    }
    return $atoms;
  }

  public function show( $id = 0 ) {
    return $this->base_get( Atom::class, $id );
  }

  public function store() {
    $this->validate( $this->request, [
      'name' => 'string|min:1|max:3|alpha_num',
      'value' => 'string|min:1|max:250|required',
    ]);
    $atom = new Atom( $this->request->all() );
    if ( $atom->save() ) {
      return response()->json([ 'status' => 'OK', ], 200);
    }
    return response()->json([ 'error' => 'Error creating ' ], 500 );
  }

  public function update( $id ) {
    $this->validate( $this->request, [
      'name' => 'string|min:1|max:3|alpha_num',
      'value' => 'numeric|min:0|max:2',
    ]);
    $atom = Atom::find( $id );
    if ( !$atom ) { return response()->json([ 'error' => 'Can not find id', ], 404 ); }
    $atom->fill( $this->request->all() );
    $atom->save();

    $rules = $atom->rules()->get();
    foreach ( $rules as $rule ) {
      Rule::solve( $rule );
    }

    return response()->json([ 'status' => 'OK', ], 200);
  }

  public function destroy( $id ) {
    $this->base_delete( Atom::class, $id );
  }

  private function pushAtom( $id ) {
    if ( !isset( $this->atomList[ $id ] ) ) {
      $this->atomList[ $id ] = false;
    }
  }

  private function checkAtom( $id ) { $this->atomList[ $id ] = true; }

  private function pushRule( $id ) {
    if ( !isset( $this->ruleList[ $id ] ) ) {
      $this->ruleList[ $id ] = false;
    }
  }

  private function checkRule( $id ) { $this->ruleList[ $id ] = true; }

  private function backtrackRule() {
    foreach ( $this->ruleList as $rule_id => $value ) {
      if ( !$value ) {
        $atoms = AtomRule::where( 'rule_id', $rule_id )->pluck( 'atom_id' )->toArray();
        foreach ( $atoms as $atom_id ) {
          $this->pushAtom( $atom_id );
        }
        $this->checkRule( $rule_id );
        $this->backtrackAtom();
      }
    }
  }

  private function backtrackAtom() {
    foreach ( $this->atomList as $atom_id => $value ) {
      if ( !$value ) {
        $rules = AtomRule::where( 'atom_id', $atom_id )->pluck( 'rule_id' )->toArray();
        foreach ( $rules as $rule_id ) {
          $this->pushRule( $rule_id );
        }
        $this->checkAtom( $atom_id );
        $this->backtrackRule();
      }
    }
  }

  public function search( $id ) {
    $atom = Atom::findOrFail( $id );
    $this->pushAtom( $atom->id );
    $this->backtrackAtom();
    $rules = [];
    foreach ( $this->ruleList as $rule_id => $value ) {
      $rule = Rule::find( $rule_id );
      array_push( $rules, $rule );
    }
    return response()->json( $rules, 200 );
  }

}
