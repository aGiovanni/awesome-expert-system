<?php

namespace App\Http\Controllers;

use Laravel\Lumen\Routing\Controller as BaseController;

class Controller extends BaseController {

  protected function base_get( $model, $id = 0 ) {
    $resp = $id ? $model::find( $id ) : $model::all();
    if ( !$resp ) {
      return response()->json([ 'error' => 'Can not find id', ], 404 );
    };
    return response()->json( $resp, 200 );
  }

  protected function base_store( $req, $model, $validations ) {
    $this->validate( $req, $validations );
    $obj = new $model;
    $obj->fill( $req->all() );
    if ( $obj->save() ) {
      return response()->json([ 'status' => 'OK', ], 200);
    }
    return response()->json([ 'error' => 'Error creating entry' ], 500 );
  }

  protected function base_update( $req, $model, $id, $validations ) {
    $this->validate( $req, $validations );

    $obj = $model::find( $id );
    if ( !$obj ) { return response()->json([ 'error' => 'Can not find id', ], 404 ); }

    $obj->fill( $req->all() );
    $obj->save();
    return response()->json([ 'status' => 'OK', ], 200);
  }

  protected function base_delete( $model, $id ) {
    if ( $model::destroy( $id ) ) {
      return response()->json([ 'status' => 'OK', ], 200);
    }
    return response()->json([ 'error' => 'Can not find id', ], 404 );
  }

}
