<?php

use Laravel\Lumen\Testing\TestCase as BaseTestCase;
use Laravel\Lumen\Testing\WithoutMiddleware;

abstract class TestCase extends BaseTestCase {

  use WithoutMiddleware;

  /**
  * Creates the application.
  *
  * @return \Laravel\Lumen\Application
  */
  public function createApplication() {
    return require __DIR__.'/../bootstrap/app.php';
  }

  public function log( $response ) { dd( $response->getContent() ); }

  public function getRandom( $model ) { return $model::inRandomOrder()->first(); }

  public function assertJsonStructure( $structure, $json_obj ) {
    $object = json_decode($json_obj);
    if ( is_array($object) ) {
      foreach ( $object as $item ) {
        foreach ( $structure as $key ) { $this->assertObjectHasAttribute( $key, $item ); }
      }
    } else {
      foreach ( $structure as $key ) { $this->assertObjectHasAttribute( $key, $object ); }
    }
  }

  public function api_can_get( $url, $structure, $status ) {
    $response = $this->call( 'GET', $url );

    $this->assertEquals( $status, $response->status() );
    if ( $structure ) {
      $this->assertJsonStructure( $structure, $response->getContent() );
    }
  }

  public function api_can_create( $url, $data, $structure, $status ) {
    $response = $this->call( 'POST', $url, $data );

    $this->assertEquals( $status, $response->status() );
    if ( $structure ) {
      $this->assertJsonStructure( $structure, $response->getContent() );
    }
  }

  public function api_can_update( $url, $update_data, $status ) {
    $response = $this->call( 'PATCH', $url, $update_data );

    $this->assertEquals( $status, $response->status() );
  }

  public function api_can_delete( $url, $data, $status ) {
    $response = $this->call( 'DELETE', $url, $data ? $data : [] );

    $this->assertEquals( $status, $response->status() );
  }

}
