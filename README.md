Instalar dependenencias con 
> composer install

modificar la variable de entorno cambiar el JWT Secret y el acceso a la BD
> cp .env.example .env

ejecutar migraciones con
> php artisan migrate:fresh

ejecutar pruebas con
> ./vendor/bin/phpunit

ejecutar como servidor
> php -S localhost:8000 -t public

